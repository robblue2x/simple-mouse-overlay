# simple-mouse-overlay

Use X11 to draw a small window beside your mouse pointer for games that have tiny or hard to see mouse cursors.

### build

    rm -rf build
    mkdir build
    cd build
    cmake ..
    make

### run

*Always run in a terminal since the only way to exit is to SIGTERM with Ctrl-C (or use a process manager)*

    cd build
    ./simple-mouse-overlay
