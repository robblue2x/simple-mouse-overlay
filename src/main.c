
#include <X11/Xlib.h>
#include <X11/extensions/XInput.h>
#include <X11/keysym.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int run = True;

long EVENT_MASK = ButtonMotionMask | Button1MotionMask | Button2MotionMask | Button3MotionMask | Button4MotionMask |
                  Button5MotionMask | ButtonPressMask | ButtonReleaseMask;

void sig_term_handler(int signum, siginfo_t *info, void *ptr) { run = 0; }

int shouldHide(Display *display) {
  int isDown = 0;
  int num_devices;
  XDeviceInfo *devices = XListInputDevices(display, &num_devices);

  for (int i = 0; i < num_devices; i++) {

    XDeviceInfo devInfo = devices[i];

    for (int j = 0; j <= devInfo.num_classes; j++) {

      if (0 == devInfo.type) {
        continue;
      }

      if (ButtonClass == devInfo.inputclassinfo[j].class) {

        XDevice *device = XOpenDevice(display, devInfo.id);
        XDeviceState *state = XQueryDeviceState(display, device);

        char *data = (char *)state->data;
        for (int k = 0; k < state->num_classes; k++) {
          XInputClass *cls = (XInputClass *)data;

          if (cls->class != ButtonClass) {
            continue;
          }

          XButtonState *but_state = (XButtonState *)cls;

          if (3 > but_state->num_buttons) {
            continue;
          }

          for (int l = 1; l <= 3; l++) {
            isDown = 8 == but_state->buttons[0] ? 1 : isDown;
          }

          data += cls->length;
        }

        XCloseDevice(display, device);

        XFreeDeviceState(state);
      }
    }
  }

  char keys_return[32];
  XQueryKeymap(display, keys_return);

  KeyCode kc2 = XKeysymToKeycode(display, XK_Control_R);
  int isCtrlRDown = (keys_return[kc2 >> 3] & (1 << (kc2 & 7)));

  return isDown || isCtrlRDown;
}

void redraw(Display *display, int screen, Window window) {
  GC gc = DefaultGC(display, screen);

  XPoint points[] = {{0, 0}, {16, 64}, {32, 32}};
  XPoint points2[] = {{0, 0}, {64, 16}, {32, 32}};
  XPoint points3[] = {{16, 64}, {56, 56}, {64, 16}, {32, 32}};

  XSetForeground(display, gc, 0xFFFF00);
  XFillPolygon(display, window, gc, points, 3, Nonconvex, CoordModeOrigin);

  XSetForeground(display, gc, 0x00FF00);
  XFillPolygon(display, window, gc, points2, 3, Nonconvex, CoordModeOrigin);

  XSetForeground(display, gc, 0x002200);
  XFillPolygon(display, window, gc, points3, 4, Nonconvex, CoordModeOrigin);
}

int main(void) {
  Display *display;
  Window window;
  int screen;
  int number_of_screens;
  Window *root_windows;
  Window window_returned;
  int root_x = 0, root_y = 0;
  int win_x = 0, win_y = 0;
  unsigned int mask_return;
  XSetWindowAttributes attributes;

  static struct sigaction _sigact;

  memset(&_sigact, 0, sizeof(_sigact));
  _sigact.sa_sigaction = sig_term_handler;
  _sigact.sa_flags = SA_SIGINFO;

  sigaction(SIGTERM, &_sigact, NULL);

  display = XOpenDisplay(NULL);
  if (display == NULL) {
    fprintf(stderr, "Cannot open display\n");
    exit(1);
  }
  number_of_screens = XScreenCount(display);

  screen = DefaultScreen(display);
  window = XCreateSimpleWindow(display, RootWindow(display, screen), 0, 0, 64, 64, 0, BlackPixel(display, screen),
                               BlackPixel(display, screen));

  attributes.override_redirect = True;
  XChangeWindowAttributes(display, window, CWOverrideRedirect, &attributes);

  XSelectInput(display, window, NoEventMask);

  XMapWindow(display, window);

  root_windows = malloc(sizeof(Window) * number_of_screens);

  for (int i = 0; i < number_of_screens; i++) {
    root_windows[i] = XRootWindow(display, i);
  }

  while (run) {
    for (int i = 0; i < number_of_screens; i++) {
      Bool result = XQueryPointer(display, root_windows[i], &window_returned, &window_returned, &root_x, &root_y,
                                  &win_x, &win_y, &mask_return);
      if (result == True) {
        break;
      }
    }

    if (shouldHide(display)) {
      XMoveWindow(display, window, -100, 0);
    } else {
      XMoveWindow(display, window, root_x + 5, root_y + 5);
    }
    redraw(display, screen, window);

    usleep(16000);
  }

  free(root_windows);

  XCloseDisplay(display);

  return 0;
}
